# 数据监控平台相关记录

## 数据层

#### 具体指标

###### 平台数据服务api接口 
------
* 数据预览 板块
    * 8组数据
        * api名称： 数据监控平台相关-数据概览值-每天更新当天数据
        * apipath： data_monitoring_platform_overview_qoq_num_di 
        * 对应表： ads_sys_data_monitoring_platform_overview_qoq_num_di   
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_overview_qoq_num_di 
        token值 fc7656c9444d46dea66255d6a836d4db   
        入参 starttime--2020-01-01
            endtime--2020-01-01
            page_num--xx
            page_size--xx
        返回值中类型说明 {
        `datatype` string COMMENT '数据类型 ', 
        `thisday_num` decimal(20,6) COMMENT '当前日期值', 
        `avg7day_num` decimal(20,6) COMMENT '前7天平均值'

            datatype的值含义{
                active_user_1day 为 活跃用户 
                jump_map 为 线下导办地图打开次数 
                localinfo_modulein_first 为 首访应用为咨询的次数
                wxshare_session 为 分享至对话框点击数
                wxshare_moments 为 分享至朋友圈点击数
                msg_push_open 为 当日打开消息推送人数
                msg_push_close 为 当日关闭消息推送人数 
                msg_push_click 为 消息推送点击率 
            }
        } 
        ```
        
------
------
* 活跃用户数 板块
    * 活跃用户数 曲线图
        * api名称： 数据监控平台相关-日活跃用户
        * apipath： data_monitoring_platform_active_user_1day
        * 对应表： ads_report_day_trend_1d 的 active_user_1d  
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_active_user_1day 
        token值 1338c5474b0d4a25bbcb2ce1c9334f3c  
        入参 starttime--2020-01-01
            endtime--2020-01-01
            page_num--xx
            page_size--xx
        ```
    * 应用访问人数排名 
        * api名称： 数据监控平台相关-各模块访问的uv与环比60天平均值的值-每天更新当天数据
        * apipath： data_monitoring_platform_allmodule_active_qoq_num_di 
        * 对应表： ads_sys_allmodule_active_qoq_num_di   
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_allmodule_active_qoq_num_di 
        token值 cd0ba9c724d6492ba3bca2d334313da0   
        入参 starttime--2020-01-01
            endtime--2020-01-01
            page_num--xx
            page_size--xx
        返回值中类型说明 {
            `moduleid` string COMMENT '模块id', 
            `moduleid_trans` string COMMENT '模块id对应中文', 
            `uv_1d` bigint COMMENT 'uv值', 
            `qoq_avg60day_1d` double COMMENT '环比60天平均值的值'
        } 
        ```
    * 咨询文章点击量排名
        * api名称： 数据监控平台相关-具体文章点击量排序-每天更新当天数据
        * apipath： data_monitoring_platform_localinfo_behavior_information_top_di 
        * 对应表： ads_localinfo_behavior_information_top_di   
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_localinfo_behavior_information_top_di 
        token值 73b05ea735834ba08b55277b59a99501   
        入参 starttime--2020-01-01
            endtime--2020-01-01
            page_num--xx
            page_size--xx
        返回值中类型说明 {
            `real_title` string COMMENT '具体文章', 
            `uv_1d` bigint COMMENT 'uv值', 
            `proportion` double COMMENT '占比'
        } 
        ```
------
------
* 线下导办 板块
    * 地图打开次数 曲线图
        * api名称： 数据监控平台相关-4类地图总pv数-每天更新当天数据
        * apipath： data_monitoring_platform_jump_map_all_pv_di
        * 对应表： ads_sys_jump_map_all_pv_di  
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_jump_map_all_pv_di  
        token值 0b98187ca7614940a414be981200996b  
        入参 starttime--2020-01-01
            endtime--2020-01-01
            page_num--xx
            page_size--xx
        返回值中类型说明 {
            `pv_1d` bigint COMMENT 'pv值', 
            `dis_active_day` decimal(10,6) COMMENT '除日活值'
        } 
        ```
    * 打开地图办事项排名--当日 
        * api名称： 数据监控平台相关-各模块跳转地图pv数-每天更新当天数据
        * apipath： data_monitoring_platform_jump_map_module_top_pv_di
        * 对应表： ads_sys_jump_map_module_top_pv_di  
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_jump_map_module_top_pv_di  
        token值 ec49f336e1fb430cbffee0d8ec7f9afa  
        入参 starttime--2020-01-01
            endtime--2020-01-01
            page_num--xx
            page_size--xx
        返回值中类型说明 {
            `moduleid` string COMMENT '模块id', 
            `moduleid_trans` string COMMENT '模块id对应中文', 
            `pv_1d` bigint COMMENT 'pv值'
        } 
        ```
    * 打开地图办事项排名--所在周
        * api名称： 数据监控平台相关-各模块跳转地图pv数-每天更新当前周数据
        * apipath： data_monitoring_platform_jump_map_module_top_pv_wi
        * 对应表： ads_sys_jump_map_module_top_pv_wi  
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_jump_map_module_top_pv_wi 
        token值 a3ff677eaa304ae5befb374fb763604f   
        入参 starttime--2020-01-01
            endtime--2020-01-01
            page_num--xx
            page_size--xx
        返回值中类型说明 {
            `moduleid` string COMMENT '模块id', 
            `moduleid_trans` string COMMENT '模块id对应中文', 
            `pv_1w` bigint COMMENT 'pv值'
        } 
        ``` 
------
------
* 咨询 板块 
    * 首访应用为咨询次数 曲线图 
        * api名称： 数据监控平台相关-首访应用为本地资讯的人数-每天更新当天数据
        * apipath： data_monitoring_platform_localinfo_modulein_first_uv_di
        * 对应表： ads_localinfo_modulein_first_uv_di  
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_localinfo_modulein_first_uv_di 
        token值 9d9b1e4375d544cfa140fed2c7b6bed8   
        入参 starttime--2020-01-01
            endtime--2020-01-01
            page_num--xx
            page_size--xx
        返回值中类型说明 {
            `uv_1d` bigint COMMENT 'uv值', 
            `dis_active_day` decimal(10,6) COMMENT '除日活值'
        } 
        ```
    * 咨询各板块点击量排名--当日 
        * api名称： 数据监控平台相关-公众号uv排序-每天更新当天数据
        * apipath： data_monitoring_platform_localinfo_behavior_account_top_di
        * 对应表： ads_localinfo_behavior_account_top_di      
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_localinfo_behavior_account_top_di 
        token值 11f6183a66604524a228a1fb594199af   
        入参 starttime--2020-01-01
            endtime--2020-01-01
            types-- 1 string类型 
            page_num--xx
            page_size--xx
        返回值中类型说明 {
            `show_name` string COMMENT '具体文章', 
            `uv_1d` bigint COMMENT 'uv值', 
            `isorder` string COMMENT '排序'
        } 
        ```
    * 咨询各板块点击量排名--所在周 
        * api名称： 数据监控平台相关-公众号uv排序-每天更新本周一到当天数据
        * apipath： data_monitoring_platform_localinfo_behavior_account_top_wi
        * 对应表： ads_localinfo_behavior_account_top_wi      
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_localinfo_behavior_account_top_wi
        token值 bd93bf26931c40529948a49f93fd911a   
        入参 starttime--2020-01-01
            endtime--2020-01-01
            types-- 1 string类型 
            page_num--xx
            page_size--xx
        返回值中类型说明 {
            `show_name` string COMMENT '具体文章', 
            `uv_1d` bigint COMMENT 'uv值', 
            `isorder` string COMMENT '排序'
        } 
        ```
    * 咨询文章访问点击量排名的 具体公众号名称
        * api名称： 数据监控平台相关-本地资讯公众号维表-每天更新全量覆盖采集
        * apipath： data_monitoring_platform_information_account_df
        * 对应表： ods_t_information_account_df      
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_information_account_df 
        token值 e88eaaad993e423fbe8ed193c8ca24a7   
        入参 
            page_num--xx
            page_size--xx
        返回值中类型说明 {
            `show_name` string COMMENT '公众号'
        } 
        ```

    * 咨询文章访问点击量排名
        * api名称： 数据监控平台相关-公众号对应的文章点击量排序-每天更新当天数据
        * apipath： data_monitoring_platform_localinfo_account_information_top_di
        * 对应表： ads_localinfo_behavior_account_information_top_di      
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_localinfo_account_information_top_di 
        token值 524046f865a440eaba7b932c2380bbae   
        入参 starttime--2020-01-01
            endtime--2020-01-01 
            page_num--xx
            page_size--xx
        返回值中类型说明 {
            `show_name` string COMMENT '公众号', 
            `real_title` string COMMENT '具体文章', 
            `pv_1d` bigint COMMENT 'pv值', 
            `proportion` double COMMENT '占比', 
            `isorder` string COMMENT '排序'
        } 
        ```
------
------
* 分享 板块 
    * api名称： 数据监控平台相关-分享到朋友圈和对话框的pv数
        * apipath： data_monitoring_platform_app2share_viewtype_pv_di
        * 对应表： ads_sys_sharetarget_app2share_viewtype_pv_di 
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_app2share_viewtype_pv_di
        token值 73cbbe448c514eac9893f1b45bb64907
        入参 starttime--2020-01-01
            endtime--2020-01-01
            page_num--xx
            page_size--xx
            types--wxshare_moments 为 分享至朋友圈;wxshare_session 为 分享至聊天框;
        ```
        

    * api名称： 数据监控平台相关-top当前天的分享到朋友圈和对话框的pv数
        * apipath： data_monitoring_platform_app2share_viewtype_module_top_di
        * 对应表： ads_sys_sharetarget_app2share_viewtype_module_top_di 
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_app2share_viewtype_module_top_di
        token值 17d5957f75f94fbcbdb19d6e66bd1790
        入参 starttime--2020-01-01
            endtime--2020-01-01
            page_num--xx
            page_size--xx
            types--wxshare_moments 为 分享至朋友圈;wxshare_session 为 分享至聊天框;
        返回值中类型说明 {
            `viewtype` string COMMENT '\'wxshare_session\'聊天框, \'wxshare_moments\'朋友圈', 
            `moduleid` string COMMENT '模块id', 
            `moduleid_trans` string COMMENT '模块id对应中文', 
            `pv_1d` bigint COMMENT 'pv值'
        } 
        ```

    * api名称： 数据监控平台相关-top当前周的分享到朋友圈和对话框的pv数
        * apipath： data_monitoring_platform_app2share_viewtype_module_top_wi
        * 对应表： ads_sys_sharetarget_app2share_viewtype_module_top_wi 
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_app2share_viewtype_module_top_wi
        token值 f919092ef07c4cdb940b3c9bba38e068 
        入参 starttime--2020-01-01
            endtime--2020-01-01
            page_num--xx
            page_size--xx
            types--wxshare_moments 为 分享至朋友圈;wxshare_session 为 分享至聊天框;
        返回值中类型说明 {
            `viewtype` string COMMENT '\'wxshare_session\'聊天框, \'wxshare_moments\'朋友圈', 
            `moduleid` string COMMENT '模块id', 
            `moduleid_trans` string COMMENT '模块id对应中文', 
            `pv_1d` bigint COMMENT 'pv值'
        } 
        ``` 

    * api名称： 数据监控平台相关-60天曲线图最大最小值
        * apipath： data_monitoring_platform_graph_maxmin_num_di
        * 对应表： ads_sys_data_monitoring_platform_graph_maxmin_num_di 
        ```
        http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/data_monitoring_platform_graph_maxmin_num_di
        token值 2cd1c93e02a54ebab59ced5aec870e02 
        入参 starttime--2020-01-01
            endtime--2020-01-01
            page_num--xx
            page_size--xx
        返回值中类型说明 {
            active_user_1day 为 日活;
            wxshare_moments 为 分享至朋友圈;
            wxshare_session 为 分享至聊天框;
            jump_map 为 地图跳转;
            localinfo_modulein_first 为 首访应用为咨询
        }
        ``` 
------

 
