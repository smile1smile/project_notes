# 数据调度中心数据驾驶舱相关记录

## 数据层

#### 具体指标

###### 平台数据服务api接口 
------
* 当月推送时序图板块
    * api名称： 消息调度中心-推送相关-当月推送时序图
    * apipath： message_dispatching_center_push_data_summary_di 
    * 对应表： ads_message_push_data_summary_di    
    ```
    http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/message_dispatching_center_push_data_summary_di  
    token值 7dec4872175e4674adfa81de03b50ad1   
    入参 starttime--2020-01-01
        endtime--2020-01-01
        page_num--xx
        page_size--xx
    返回值中类型说明 {
        `data_type` string COMMENT '数据分类 {push, site, sms}', 
        `data_num` decimal(25,5)转为string COMMENT '数值'
        `dt` string COMMENT '平台计算日期'
        data_type 的值含义{
            push_pv 表示'push的数据'，
            site_pv 表示'站内信'，
            sms_pv 表示'短信' ，
            click_rate 表示'消息点击率'
        }
    } 
    ```
------
------
* 当月推送统计饼图板块
    * api名称： 消息调度中心-推送相关-当月推送饼状图
    * apipath： message_dispatching_center_push_data_summary_month_di 
    * 对应表： ads_message_push_data_summary_month_di    
    ```
    http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/message_dispatching_center_push_data_summary_month_di 
    token值 743a004b32d846bdb36358531acbee9a   
    入参 starttime--2020-01-01
        page_num--xx
        page_size--xx
    返回值中类型说明 {
        `data_type` string COMMENT '数据分类 {push, site, sms}', 
        `data_num` decimal(25,5)转为string COMMENT '数值'
        `dt` string COMMENT '平台计算日期'
        data_type 的值含义{
            push_pv 表示'push的数据'，
            site_pv 表示'站内信'，
            sms_pv 表示'短信' 
        }
    } 
    ```
------
------
* 整体数据概览板块
    * api名称： 消息调度中心-推送相关-整体数据概览
    * apipath： message_dispatching_center_push_data_overview_qoq_num_di 
    * 对应表： ads_message_push_data_overview_qoq_num_di    
    ```
    http调用链接：http://10.192.129.129:10073/easy-data-api/mynj_prod/screen/message_dispatching_center_push_data_overview_qoq_num_di 
    token值 ee5f309b1fe64ceeac957a46746a3a74   
    入参 starttime--2020-01-01
        page_num--xx
        page_size--xx
    返回值中类型说明 {
        `data_type` string COMMENT '数据分类 {push, site, sms}', 
        `data_num` decimal(25,5)转为string COMMENT '数值'
        `dt` string COMMENT '平台计算日期'
        data_type 的值含义{
            send_all_pv 表示'发送的总pv'，
            send_all_uv 表示'发送的总uv'，
            send_success_rate 表示'发送成功率' 
            push_click_rate 表示'push的点击率' 
            site_click_rate 表示'site站内信息的点击率' 
            all_click_rate 表示'总的点击率' 
        }
    } 
    ```
------
